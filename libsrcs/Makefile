SHELL :=/bin/bash
SHELL := /bin/bash
RULES         = $(CURDIR)/Rules_make/Rules.make
include $(RULES)

OBJDIR        = $(CURDIR)/$(ODIR)
MODDIR        = $(CURDIR)/$(MDIR)
LIBDIR        = $(CURDIR)/$(LDIR)
DOXDIR        = $(CURDIR)/$(DDIR)
DOCDIR        = $(CURDIR)/doc
RUNDIR        = $(CURDIR)/runs

INCLUDE       += -I$(MODDIR)

FFLAGS        = $(OFLAGS) $(MODEL)  $(INCLUDE) 

LIBSRCS       = 

LIBS          =

LIBNAME       = libglobals.a

MODNAME       = 

MAKEFILE      = Makefile

SRCS	      = 000modKindDeclaration.f90 00modGlobals.f90 01modNorms.f90\
		02modTiming.f90 04modScratch.f90 05modDataSequence.f90\
		07modTimeInputs.f90 08modTimeOutputs.f90 09modDmkOdeData.f90\
		10modGaussQuadrature.f90

OBJS	      = 000modKindDeclaration.o 00modGlobals.o 01modNorms.o\
		02modTiming.o 04modScratch.o 05modDataSequence.o\
		07modTimeInputs.o 08modTimeOutputs.o 09modDmkOdeData.o\
		10modGaussQuadrature.o

PRJS= $(SRCS:jo=.prj)

OBJECTS        = $(SRCS:%.f90=$(OBJDIR)/%.o)

MODULES        = $(addprefix $(MODDIR)/,$(MODS))

LIBRARY        = $(LIBDIR)/$(LIBNAME)


.SUFFIXES: .prj .f90

print-%  : 
	@echo $* = $($*)

.f.prj:
	ftnchek -project -declare -noverbose $<

.f90.o:
	$(FC) $(FFLAGS) $(INCLUDE) -c  $< 

all::
		@make dirs
		@make $(LIBRARY) 

$(LIBRARY):     $(LIBS) $(MODULES) $(OBJECTS)
		@echo "Moving $(LIBNAME) to $(LDIR)"
		@ar $(ARFLAGS) cru $(LIBRARY) $(OBJECTS)

$(LIBS):
		@set -e; for i in $(LIBSRCS); do cd $$i; $(MAKE) --no-print-directory -e CURDIR=$(CURDIR); cd ~-; done

$(OBJECTS): $(OBJDIR)/%.o: %.f90 
		$(FC) $(CPPFLAGS) $(FFLAGS) -o $@ -c $<

.PHONY:         doc

doc: 
		@-rm -f $(DOXDIR)/README.md
		@cat $(DOXDIR)/README.tpl $(DOXDIR)/*.txt > $(DOXDIR)/README.md
		doxygen $(DOXDIR)/DoxygenConfigFortran 

dirs: 
		@-mkdir -p $(DOCDIR) $(OBJDIR) $(MODDIR) $(LIBDIR)

clean-emacs:
		@-rm -f $(CURDIR)/*.*~ 
		@-rm -f $(CURDIR)/*\#* 

debug:;         @make "FFLAGS=$(DFLAGS) $(MODEL) $(INCLUDE)" 

openmp:;         @make "FFLAGS=$(OFLAGS) $(OPENMP) $(MODEL) $(INCLUDE)"

clean:;		@rm -f $(OBJECTS) $(MODULES) 
		@set -e; for i in $(LIBSRCS); do cd $$i; $(MAKE) --no-print-directory clean; cd ~-; done

clobber:;	@rm -f $(OBJECTS) $(MODULES) 
		@-rm -rf $(DOCDIR)/latex/* $(DOCDIR)/html/* $(OBJDIR) $(MODDIR) $(LIBDIR)
		@-rm -f $(CURDIR)/*.*~ 
		@-rm -f $(CURDIR)/*\#* 

.PHONY:		mods

mods:;		
		./scripts/f90_mod_deps.py $(SRCS) > Rules_make/dependencies
		./scripts/f90_source_deps.py $(SRCS)>> Rules_make/dependencies


# this file is created by "make mods" It needs to exist (even if empty)
# for the makefile to work
include Rules_make/dependencies
