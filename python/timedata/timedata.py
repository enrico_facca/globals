#!/usr/bin/env python
import numpy as np
import sys
import os



#
# Fucntion for a fast reading from file of timedata .
# For steady-state data only 
#
def read_steady_timedata(filename):
   fin=open(str(filename), 'r')
   lines = fin.readlines()
   dimdata=int(lines[0].split()[0])
   ndata=int(lines[0].split()[1])
   data=np.zeros([ndata,dimdata])   
   ninputs= int(lines[2].split()[0])
   for i in range(ninputs):
      line=lines[3+i]
      idata=int(line.split()[0])
      data[idata-1,:]=[float(w) for w in line.split()[1:]]
   fin.close()
   return data;

#
# Fucntion for a fast reading from file of timedata .
# For steady-state data only 
#
def get_data(asked_time,filename):
   # open file
   fin=open(str(filename), 'r')
   # convert into strings
   input_lines = fin.readlines()
   # allcoate space
   [dimdata,ndata]=read_head(input_lines)
   data=np.zeros([dimdata,ndata])
   data_old=np.zeros([dimdata,ndata])
   
   # initialize counter
   iline=1
   portion=0
   EOF=False

   # read first time
   time,nnz,EOF,data_old=read_portion(input_lines,iline,data_old)
   time_old=time
   # start reading cycle from second portion
   while (time < 1e+30):
      time,nnz,EOF,data=read_portion(input_lines,iline,data)
      #print(time_old,time,nnz,asked_time)
      # same times
      #if ( time == time_old):
      #   return data
      if ( time > asked_time):
         # asked time found 
         # linear interpolation
         tprec=(asked_time-time_old)/(time-time_old)
         out_data=(1-tprec) * data_old + tprec * data
         return out_data; 
      else:
         time_old=time
         data_old=data      
      # next
      iline=iline+2+nnz
      portion=portion+1
      if ( EOF ):
           return data
   
   # close file
   fin.close()
   return

#
# returns the list of time reported in file
#
def get_times(filename):
   with open(filename) as f:
      datafile = f.readlines()
   times=[]
   for line in datafile:
      if 'time' in line:
         times.append(float(line.split()[1]))
   return np.asarray(times)


#
# Fucntion for a fast writing to file of timedata .
# For steady-state data only 
#
def write_steady_timedata(filename,data):
   file_out=open(str(filename), 'w')
   try:
      dimdata=data.shape[1]
      ndata=data.shape[0]
   except:
      dimdata=1
      ndata=len(data)
      
   file_out.write(
      str(dimdata) + " " + 
      str(ndata) + " !  dim ndata" 
      +"\n")
   nrm_data=np.zeros(ndata)
   for i in range(ndata):
      nrm_data[i]=np.linalg.norm(data[:][i])
   nnz=(abs(nrm_data) != 0.0).sum()
   #print nnz.shape
   file_out.write("time    -1.0e-30 \n")
   file_out.write(str(ndata)+" \n")
   try:
      for i in range(ndata):
         #if ( np.sum(np.abs(data[:][i])) != 0.0):
         file_out.write(str(i+1)+" " + 
                        " ".join(map(str,data[:][i])) +"\n")
   except:
      for i in range(ndata):
         #if ( np.sum(np.abs(data[i])) != 0.0):
         file_out.write(str(i+1)+" " + 
                        str(data[i]) +"\n")
      
   file_out.write("time  1.0e+30 \n")
   file_out.close()
   return;

def write_zero_timedata(filename,dimdata,ndata):
   file_out=open(str(filename), 'w')
   file_out.write(
      str(dimdata) + " " + 
      str(ndata) + " !  dim ndata" 
      +"\n")
   file_out.write("time    -1.0e-30 \n")
   file_out.write(str(0)+" \n")
   file_out.write("time  1.0e+30 \n")
   file_out.close()
   return;
    

#
# Write time data to file
#
#
# example of use in test.py
# 
def write2file(file_out,time,head,data,steady=False):
   try:
      dimdata=data.shape[1]
      ndata=data.shape[0]
   except:
      dimdata=1
      ndata=len(data)
   if ( head ) :
      file_out.write(
         str(dimdata) + " " + 
         str(ndata) + " !  dim ndata" 
         +"\n")
   else:
      if (dimdata>1):
         nrm_data=np.zeros(ndata)
         for i in range(ndata):
            nrm_data[i]=np.linalg.norm(data[:][i])
            nnz=(abs(nrm_data) != 0.0).sum()
            #print nnz
            file_out.write("time    "+ str(time)+" \n")
            file_out.write(str(nnz)+" \n")
            for i in range(ndata):
               if ( np.sum(np.abs(data[:][i])) != 0.0):
                  file_out.write(str(i+1)+" " + 
                                 " ".join(map(str,data[:][i])) +"\n")
               if( steady ):
                  file_out.write("time " + str(1.e30)+"\n")
      else:
         nnz=(abs(data) != 0.0).sum()
         #print nnz
         file_out.write("time    "+ str(time)+" \n")
         file_out.write(str(nnz)+" \n")
         for i in range(ndata):
            if ( np.abs(data[i]) != 0.0):
               file_out.write(str(i+1)+" "+str(float(data[i]))+"\n")
            if( steady ):
               file_out.write("time " + str(1.e30)+"\n")
                   

#
# read time data to file
#
#
# example of use in test.py
# 
def read_head(input_lines):
   dimdata=int(input_lines[0].split()[1])
   ndata=int(input_lines[0].split()[0])
   return dimdata, ndata;

#
# read time data to file
#
#
# example of use in test.py
# 
def write_head(file_out,data):
   file_out.write(
            str(data.shape[1]) + " " + 
            str(data.shape[0]) + " !  dim ndata" 
            +"\n")
   return ;

#
# example of use in test.py
# 
def read_portion(lines,iline,data):
   EOF=False
   try:
      time=float(lines[iline].split()[1])
      if (time < 1.0e+30) :
         nnz=int(lines[iline+1].split()[0])
         for i in range(iline+2,iline+nnz+2):
            line=lines[i]
            idata=int(line.split()[0])
            data[idata-1,:]=[float(w) for w in line.split()[1:]]
      else:
         nnz=0
   except: 
       EOF=True
       time = 1.0e30
       nnz=0
   return time,nnz,EOF,data;

#
# example of use in test.py
# 
def write_portion(file_out,time,data):
   dimdata=data.shape[1]
   ndata=data.shape[0]
   nrm_data=np.zeros(ndata)
   for i in range(ndata):
      nrm_data[i]=np.linalg.norm(data[:][i])
   nnz=(abs(nrm_data) != 0.0).sum()
   #print nnz
   file_out.write("time    "+ str(time)+" \n")
   file_out.write(str(nnz)+" \n")
   for i in range(ndata):
      if ( np.sum(np.abs(data[:][i])) != 0.0):
         file_out.write(str(i+1)+" " + 
                        " ".join(map(str,data[:][i])) +"\n")
   

def balance(data,option=None):
    if (option==None):
        option='plus'
    plus=data[data>0].sum()
    minus=abs(data[data<0].sum())

    
    
    balanced_data=np.zeros(len(data))

    if (option == 'plus'):
        print (str(option)+'Positive part preserved | Negative part scaled')
        for i in range(len(data)):
            if ( data[i] > 0.0 ):
               balanced_data[i] = data[i]
            if ( data[i] < 0.0 ) :
               balanced_data[i] = data[i]*plus/minus

    if (option == 'minus'):
        print (str(option)+'Positive part scaled | Negative part preserved')
        for i in range(len(data)):
            if ( data[i] > 0.0 ):
                balanced_data[i] = data[i]*minus/plus
            if ( data[i] < 0.0 ) :
                balanced_data[i] = data[i]

    print ( 'Mass positive part:='+str(plus))
    print ( 'Mass negative part:='+str(minus))
    print ( 'Input  unbalanced :='+str(data.sum()))
    print ( 'Output unbalanced:='+str(balanced_data.sum()))

    return balanced_data;

