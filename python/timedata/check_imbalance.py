import timedata as td
import numpy as np
import sys

filename=sys.argv[1]

try:
        tolerance=float(sys.argv[2])
except:
        tolerance=1.e-15
        
#
# read all input lines
#
infile = open(filename, 'r')
input_lines = infile.readlines()
infile.close()

#
# read dimensions and initialiaze data
#
ndim,ndata=td.read_head(input_lines)
data=np.zeros([ndim,ndata])


#
# read sequentially all portion of lines 
#

# initialize counter
iline=1
portion=0
EOF=False

# read first portion and get data
time,nnz,EOF,data=td.read_portion(input_lines,iline,data)
iline=iline+2+nnz
portion=portion+1

imbalance=np.sum(data)
#if ( abs(imbalance) > tolerance ):
print('time = '+str(time)+' sum data ='+ str(np.sum(data)))
#exit()


# start reading cycle from second portion
while (time < 1e+30):
        time,nnz,EOF,data=td.read_portion(input_lines,iline,data)
        iline=iline+2+nnz
        portion=portion+1
        if ( EOF ):
           break
        imbalance=np.sum(data)
        if ( abs(imbalance) > tolerance ):
                print('time = '+str(time)+' sum data ='+ str(np.sum(data)))
                exit()
