#!/usr/bin/env python
import numpy as np
import sys
import timedata

##################################################
# Program to read a steady-state data 'rhs' and build
# a new data 'rhs_balanced' with zero average 
# i.e.: sum(rhs_balanced) = 0.
# Only the negativa part is scaled.
#
# Usage pyhton balance <filein> <fileout>
#   where :
# filein  :: path of the imbalanced steady-state data
# fileout :: path of the balanced steady-state data
##################################################


#
# read file name for input ( unbalanced) and output( balance) 
#
foriginal=sys.argv[1]
fbalanced=sys.argv[2]


#
# read data
#
rhs = timedata.read_steady_timedata(foriginal)
source = np.where(rhs>0,rhs,0.0);
sink = np.where(rhs<0,-rhs,0.0);
print ('Imbalance ='+str(np.sum(rhs)))

#
# balance and write
#
msource = np.sum(source)
msink = np.sum(sink)
sink = (sink /msink)*  msource 
rhs = source - sink
print ('New imbalance ='+str(np.sum(rhs)))
timedata.write_steady_timedata(fbalanced,rhs)
