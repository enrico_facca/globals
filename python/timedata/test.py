#!/usr/bin/env python
import numpy as np
import timedata

##################################################
# example of use of function tiemdata.write2file
# We create a steady-state data of dimension (2,5)
# with only due nonzero entries
##################################################


##### define time bound and data dimension
tzero = 0
tmax = 0.2
rhs=np.zeros([5,2])

###### write head of files #####
steady_data =False
time = tzero
fileID=open('rhs.dat', 'w')
timedata.write2file(fileID,time,True,rhs)

###### cycle in time and write to files #####
while ( (time <= tmax) and (not steady_data ) ) :
    # define data at given time 
    rhs[:,:] = 0.0
    rhs[0,1] = np.sin(time) 
    rhs[4,1] = np.cos(time)

    # write into file
    timedata.write2file(fileID,time,False,rhs)

    # define next time
    time=time+0.05
    
    # define if in steady_data
    steady = False
fileID.close()

