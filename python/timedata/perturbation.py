#!/usr/bin/env python
import numpy as np
import timedata
import random


##### read rhs dimension and nnz zeros
rhs=timedata.read_steady_timedata('example_rhs.dat')
list_of_nnz=[]
for i in range(len(rhs)):
    if (abs(rhs[i]) > 1e-20 ) :
       list_of_nnz.append(i)



##### define time bound and data dimension
tzero = 0.0
tmax = 1000
perturbation = rhs * 0.0

###### write head of files #####
steady_data =False
time = tzero
fileID=open('perturbation_damped.dat', 'w')
fileID2=open('rhs_perturbation_damped.dat', 'w')
timedata.write2file(fileID,time,True,perturbation)
timedata.write2file(fileID2,time,True,perturbation)

###### cycle in time and write to files #####
while ( (time <= tmax) and (not steady_data ) ) :
    # define data at given time 
    sum_per=0.0
    for i in range(len(list_of_nnz)-1):
        per=np.exp(-0.1*time)*random.uniform(-0.05,0.05)
        sum_per=sum_per +per
        perturbation[list_of_nnz[i],0] = per
    perturbation[list_of_nnz[-1],0]=-sum_per
    
    print('per',perturbation[list_of_nnz[:],0])
    print('sum',sum(rhs[:,0]))

    
        
    # write into file
    timedata.write2file(fileID,time,False,perturbation)
    timedata.write2file(fileID2,time,False,perturbation+rhs)

    # define next time
    time=time+0.02
    
    # define if in steady_data
    steady = False
fileID.close()
