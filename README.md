# README #

### What is this repository for? ###
Fortran library for the declaration of global variables and methods
such as precision declaration(single, double), universal constant
(zero,one, pi), sort algorithms, input/ouput procedures, etc.
Part of the fortran code can be used via python once wrapped via
f90wrap, included as git submodule.

* Version 2.0

## How do I get set up?
### Dependencies
  * PROGRAMS
    * GNU Fortran compiler compatible with FORTRAN 2003 (tested with gfortran v>=4.8).
    Default path is assumed to be "/usr/bin/gfortran"
    * Cmake ( version >= 2.8)  
  * EXTERNAL LIBRARIES
    * Blas library
    * Lapack library

### Dependencies for fortran-python interface
  * PYTHON >=3.6 and PACKAGES 
    * numpy (version 1.18.2 tested/ version>=1.19.x gave issues )
    * f90wrap 
  
### Compile pure Fortran sources

In Unix-base dsystem use the classical Cmake commands to compile
libraries and programs:
```
  mkdir build/;
  cd build;
  cmake ../; 
  make
```

Compiling options are:
* RELEASE (default)
* DEBUG
* IEEE
* USER
that can be invoked adding the cmake flag

```
cmake -DBUILD_TYPE="compiling option" .. ;  
```

### Compile Fortran-python interface (always in the build/ directory):

```
make f2py_interface_globals
```

### Troubleshooting

In case your gfortran compiler path is not "/usr/bin/gfortran" pass the absoulte path 

```
cmake -DMy_Fortran_Compiler="absolute path to gfortran" .. ;
```

In case of errors, in particular with the fortran-python interface, try
to pass the path of the directories containg the blas and lapack libraries using the
cmake flag:

```
cmake -DBLAS_DIR="absolute path for blas library" -DLAPACK_DIR="absolute path for lapack library" ..; 
```

In order to diagnose the error sources use also 

```
cmake .. VERBOSE=1;
```

* Configuration
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Repositories owners:
  * Enrico Facca (enrico.facca@sns.it)
  * Mario Putti (putti@math.unipd.it)
* Other community or team contact