!> Example of use for finding the square-root solving x^2-a=0
!>
!> ! controls flag
program test_newton
  use KindDeclaration
  use Globals
  integer             :: flag, info
  real(kind=double)   :: x(1), increment(1), a
  real(kind=double)   :: fnewton(1),jacobian
  type(newton_input)  :: ctrl
  type(newton_output) :: info_newton


  !nargs = command_argument_count()
  !call  get_command_argument(1, path_data,status=res)

  !
  ! set data and initial guess
  !
  a=two
  x=four

  !
  ! set controls
  !
  ctrl%tolerance_nonlinear=1.0d-12

  !
  ! starting flags
  !
  flag=1
  info=0
  

  !
  ! newton cycle
  !
  write(*,*) 'Iter - |x^2-a|-  x  - sqrt(a) '
  do while ( ( flag .gt. 0 ) .and. (info .eq. 0) )
     select case ( flag)
     case (1)
        ! assembly Fnewton
        fnewton(1)=x(1)**2-a
     case (2)
        ! compute norm of F
        ctrl%fnewton_norm=abs(fnewton(1))
        write(*,'(I4,1x,3(1pe9.2,1x))') info_newton%current_newton_step, ctrl%fnewton_norm, x, sqrt(a)
     case (3)
        ! find increment solving
        ! jacobian inc = -F
        jacobian=2.0*x(1)
        increment(1)=-fnewton(1)/jacobian

        ! set step-size (<one for Damped-Newton)
        ctrl%alpha=one
     end select
     ! reverse communication newton
     call newton_raphson_reverse_with_dt(&
          flag,info, 1, x, increment,&
          ctrl,info_newton)
  end do

end program test_newton
