program integer_sort_and_unique
  use Globals
  implicit none
  integer :: n=10
  integer :: array(10)
  integer :: nunique
  array=(/1,2,8,2,2,3,9,6,2,2/)

  write(*,*) 'Original array'
  write(*,'(10I2)') array
  call isort(n,array)
  write(*,*) 'Sorted array'
  write(*,'(10I2)') array

  call unique_of_sorted(n,array,nunique)
  write(*,'(a,I2)') 'Unique elements:', nunique
  write(*,*) 'Unique array:'
  write(*,'(10I2)') array(1:nunique)

end program integer_sort_and_unique
  
  
